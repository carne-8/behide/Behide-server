# ⚡ Behide server

A server in [F#](https://fsharp.org/) that manages the multiplayer for the game [Behide](https://gitlab.com/carne-8/behide).

<img src="./Assets/Presentation card.png"
     alt="Behide, open source video game"
     style="border-radius: 10px" />