namespace BehideServer.Interop.Tcp

[<RequireQualifiedAccess>]
type MsgHeader =
    /// No content
    | Ping = 1uy
    /// <summary>
    /// <para>Create room</para>
    /// <para>Content: The username of the game creator (UTF8)</para>
    /// </summary>
    | CreateRoom = 2uy

module MsgHeader =
    let inline fromByte byte : MsgHeader = LanguagePrimitives.EnumOfValue byte


type Msg =
    { Header: MsgHeader
      Content: byte [] }

    static member create header content = { Header = header; Content = content }

    member this.toBytes() =
        this.Content
        |> Array.append [| byte this.Header |]

    static member toBytes(msg: Msg) = msg.toBytes ()

    static member fromBytes (bytes: #seq<byte>) =
        { Header = bytes |> Seq.head |> MsgHeader.fromByte
          Content = bytes |> Seq.tail |> Seq.toArray }

[<RequireQualifiedAccess>]
type MsgContentParser =
    /// <summary></summary>
    /// <returns>The username of the room's creator</returns>
    static member CreateRoom(msgContent: byte []) =
        System.Text.Encoding.UTF8.GetString msgContent