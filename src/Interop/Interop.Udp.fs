namespace BehideServer.Interop.Udp

[<RequireQualifiedAccess>]
type MsgHeader =
    /// No content
    | Ping = 1uy

module MsgHeader =
    let inline fromByte byte : MsgHeader = LanguagePrimitives.EnumOfValue byte


type Msg =
    { Header: MsgHeader
      Content: byte [] }

    static member create header content =
        { Header = header
          Content = content }

    member this.toBytes () = this.Content |> Array.append [| byte this.Header |]
    static member toBytes (msg: Msg) = msg.toBytes()

    static member fromBytes (bytes: #seq<byte>) =
        { Header = bytes |> Seq.head |> MsgHeader.fromByte
          Content = bytes |> Seq.tail |> Seq.toArray }