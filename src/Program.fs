﻿module BehideServer.App

open System
open System.Net
open Serilog

let listenPort = 28000

#if DEBUG
let isRelease = false
#else
let isRelease = true
#endif

[<EntryPoint>]
let main _ =
#if DEBUG
    Console.Clear()
#endif

    let log =
        LoggerConfiguration()
            .MinimumLevel.Debug()
            .WriteTo.Console()
            .WriteTo.Conditional(
                (fun _ -> isRelease),
                (fun logger -> logger.File("logs/log.txt", rollingInterval = RollingInterval.Day) |> ignore)
            )
            .CreateLogger()

    let localIP =
        Dns.GetHostName()
        |> Dns.GetHostEntry
        |> fun x -> x.AddressList
        |> Array.find (fun x -> x.AddressFamily = Sockets.AddressFamily.InterNetwork)

    let localEP = new IPEndPoint(localIP, listenPort)

    log.Information("Starting at {0}", localIP)

    [ Listener.Udp.startListener log localEP |> Async.AwaitTask
      Listener.Tcp.startListener log localEP ]
    |> Async.Parallel
    |> Async.Ignore
    |> Async.RunSynchronously


    let mutable keyPressed = Console.ReadKey true

    while keyPressed.Key <> ConsoleKey.C
          && keyPressed.Modifiers <> ConsoleModifiers.Control do
        keyPressed <- Console.ReadKey true

    0