module BehideServer.Listener.Tcp

open BehideServer.Interop.Tcp
open System.Net
open Serilog
open SuperSimpleTcp

let onClientConnected (log: ILogger) (tcp: SimpleTcpServer) (event: ConnectionEventArgs) =
    log.Information("[TCP] New connection: {0}", event.IpPort)

let onClientDisconnect (log: ILogger) (tcp: SimpleTcpServer) (event: ConnectionEventArgs) =
    log.Information("[TCP] {0} disconnected because {1}", event.IpPort, event.Reason)

let onData (log: ILogger) (tcp: SimpleTcpServer) (event: DataReceivedEventArgs) =
    let msg = event.Data |> Msg.fromBytes

    match msg.Header with
    | MsgHeader.Ping ->
        let msg = Msg.create MsgHeader.Ping [||] |> Msg.toBytes
        tcp.SendAsync(event.IpPort, msg) |> ignore
    | _ -> ()

let startListener (log: ILogger) (endPoint: IPEndPoint) =
    async {
        let tcp = new SimpleTcpServer(endPoint |> string)

        tcp.Events.ClientConnected.Add (onClientConnected log tcp)
        tcp.Events.ClientDisconnected.Add (onClientDisconnect log tcp)
        tcp.Events.DataReceived.Add (onData log tcp)

        tcp.Start()
        log.Information("[TCP] Started on port {0}", endPoint.Port)
    }