module BehideServer.Listener.Udp

open Serilog
open System.Net
open System.Net.Sockets
open BehideServer.Interop.Udp

let proceedMsg (log: ILogger) (udp: UdpClient) (res: UdpReceiveResult) =
    task {
        let msg = res.Buffer |> Msg.fromBytes
        let remoteEP = res.RemoteEndPoint

        match msg.Header with
        | MsgHeader.Ping ->
            let response = Msg.create MsgHeader.Ping [||] |> Msg.toBytes
            udp.SendAsync(response, remoteEP) |> ignore
        | byte ->
            log.Warning("[UDP] Unexpected msg header: {0}", byte)
    }

let startListener (log: ILogger) (endPoint: IPEndPoint) =
    task {
        let udp = new UdpClient(endPoint)
        log.Information("[UDP] Started on port {0}", endPoint.Port)

        while true do
            let! res = udp.ReceiveAsync()
            res |> proceedMsg log udp |> ignore
    }